# Reset the root password  

Reboot the machine. Press e to edit the boot selection.  

Add rd.break to the end of the line (usually after "quiet")  

mount | grep sysroot  
mount -o remount,rw /sysroot/  
mount | grep sysroot  

You should now see rw instead of ro access.  

chroot /sysroot  

passwd  

touch /.autorelabel  


Also see:  
[Recover root password:linuxconfig.org](https://linuxconfig.org/redhat-8-recover-root-password)
or:  
[reset-forgotten-root-password-rhel-8-centos-8](https://www.techbrown.com/reset-forgotten-root-password-rhel-8-centos-8/)

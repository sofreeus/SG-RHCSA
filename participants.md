| Spoken Name | Mattermost Handle | Role, certs, etc... |
| ----------- | ----------------- | ------------------- |
| Andrew      | albionandrew      | RHCE 6              |
| Chris       |                   | 8 years RHEL 6&7    |
| Dustin      | boyddl            | RHCA through 7      |
| Eupheng     | eupvue3           |                     |
| Mark        | marksallee        | Linux SA 20 years   |
| Neil        | nmcousineau       | Developer 10 years  |
| Tiffany     | f0rtun4m4j0r      | SysAdmin            |
| David       | dlwillson         | DevOps              |

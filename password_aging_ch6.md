# Password aging with chage, password, and usermod - Chapter 6  

chage  
-d (--lastday)  
-d 0 = force reset on next login  
-i (--inactive)  
-l = list  
-m = mindays  
-M = maxdays  
-E = expiredate  

passwd  
-x (maximum)  
-n (minimum)  
-e = expire and force change  
-S = status  
-d = delete pw  
-i = inactive  

usermod  
usermod -L username =  lock account  
usermod -U username = unlock account  
passwd -l = lock account  
passwd -u = unlock account  
groupadd -o  = lets you share a group id across more than one group  

/etc/login.defs contains defaults on password aging  




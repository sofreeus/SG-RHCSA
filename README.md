[Exam Info](https://www.redhat.com/en/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam)

[Book Info](https://www.nixeducation.com/detail.php?bid=MTk=)
- [Helpful Videos](https://www.nixeducation.com/video.php)

[7 tips for passing certification exams](https://www.redhat.com/en/blog/7-tips-passing-red-hat-certification-exam)

[Exam Objectives for RHCSA:](https://www.redhat.com/en/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam?section=Objectives)

Exercise Ideas:

Lose the root password. This will help with the objectives around targets and interrupting the boot process.

Create a webserver. Install, configure, and enable httpd. This will help with package management, file ownership and permissions, and service management.

Create a name-based website in /srv/. This will give practice with SELinux.

Create a port-based website. This too, will give practice with SELinux.

[Password aging, change, passwd, user locking](password_aging_ch6.md)

[File permissions review: chmod, umask, setgid, acls](file_permissions_review_ch4.md)

[Firewalld review](firewalld_review.md)

[LVM review](lvm.md)

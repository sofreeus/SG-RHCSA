# firewalld - Chapter 20

firewalld zones:  
- trusted - allow all incoming  
- internal - reject all incoming except allowed  
- home - reject all incoming except allowed  
- work - reject all incoming except allowed
- dmz - reject all incoming traffic except allowed - in publicly accessible demilitarized zones  
- external - reject all traffic except allowed. Outgoing IPV4 is masqueraded  
- public - reject all incoming except allowed. Default zone for any newly added interfaces
- block - reject all incoming with icmp-host-prohibited  
- drop - drop all incoming traffic without responding with ICMP errors - for highly secure places  

Where are firewalld zones stored?  
/etc/firewalld/zones or /usr/lib/firewalld/zones  
Example: /usr/lib/firewalld/zones/public.xml  

firewalld "services" allow you to activate and deactivate specific rules.  
Where are firewalld service config files stored?  
/usr/lib/firewalld/services or /etc/firewalld/services  


Notes from session with Dustin  

Dustin reviewed the cockpit graphical tool for editing firewalld, which he says that he used on his cert exam.
https://cockpit-project.org/running.html  
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_systems_using_the_rhel_8_web_console/getting-started-with-the-rhel-8-web-console_system-management-using-the-rhel-8-web-console#what-is-the-RHEL-web-console_getting-started-with-the-rhel-8-web-console
- Web console is on port 9090  
- When using web console, select the box for "reuse my password for privileged tasks"  - it's important for access  
- sudo yum install cockpit  
- sudo systemctl enable --now cockpit.socket 
- sudo systemctl status cockpit  
- sudo systemctl status cockpit.socket  

 

sudo yum install firewall-config  
sudo firewall-cmd --add-service=cockpit --permanent  
$ firewall-cmd --reload  

RHEL 8 using and configuring firewalls - https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/securing_networks/using-and-configuring-firewalls_securing-networks  

systemctl status firewalld  or  sudo firewall-cmd --state
sudo firewall-cmd --get-default-zone
  


A neat trick: sudo systemctl --now enable firewalld - both enables service at boot and starts it now  

See Exercise 20-1: Add Services and Ports  

Review man pages:  
man firewall-cmd  

systemctl status nftables (replaces iptables/netfilter)  
sudo firewall-cmd --reload  

#After adding service to zone, make it permanent (works after boot):  
sudo firewall-cmd --zone=internal --add-service=https --permanent  
firewall-cmd --add-permanent  



Review how to apply selinux contexts  
semanage port -l | grep 'http'  

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/securing_networks/using-and-configuring-firewalls_securing-networks  

# File permissions  

chmod -v is recommended since it will show the updated permissions instantly  

Symbolic (ugo/rwx) versus octal (664)  

Octal form for permissions: 4 (read) 2 (write) 1 (execute)  

umask 
umask permission values - 3 digits  
default umask for root user: 0022   for normal users: 0002  

umask -S  = display umask of a file in octal form  

Calculate default permissions:  
file: initial permissions:      666   
 umask:                         - 002   
default permissions:            664  

directory: initial permissions: 777  
 umask:                         - 027  
default permissions: 	    	750  

Predefined initial permissions for file: 666 or rw-rw-rw  
				for folders: 777 or rwxrwxrwx  
				
Special permission bits: three types:  

1. setuid - set user ID on execution, it is a security tool that permits users to run certain programs with escalated privileges.  
- example /usr/bin/su.  
- Reapply with: chmod -v +4000 filename; or  chmod 4755 filename  

2.setgid - example /usr/bin/write  
- Remove: chmod g-s filename. Reapply: chmod g+s  
- Reapply without changing other permissions: chmod -v +2000 filename  
- setgid shared groups - inherit directory's owning group instead of creator's group  
See Exercise 4-5  Set up Shared Directory for Group Collaboration  

3. sticky bit - example /tmp  
- Remove: chmod o-t filename. Reapply: chmod o+t filename  
- Restore: chmod -v +1000  filename.  Or: chmod 1777 filename  
See Exercise 4-6 : Test the Effect of Sticky Bit  


ACLs Access Control Lists  
getfacl and setfacl  
setfacl:  
-b remove all access  
-d default  
-k remove all default  
**-m set or modify ACLs**  
-n prevent recalculation of mask  
-R apply recursively  
-x remove an access acl  
setfacl u:UID:perms  
setfactl g:GID:perms  

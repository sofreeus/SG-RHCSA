Here are some proposed topics that looked interesting, we can go over these
and decide which ones are helpful in your training as well.

From the Ghori book:

Password aging - chage, usermod, passwd - Chapter 6  
Configure IPv4 and IPv6, nmcli/nmtui - Chapter 17  
- [Challenge Lab]   do Chapter 17, Lab 17-1 - add new interface with nmcli  

Multi-user targets, switching users - Chapter 6  
Input/output redirection > >> | 2>  - Chapter 7  
Identify CPU, memory intensive processes - Chapter 8  
Manage Tuning Profiles - Chapter 12  

File ACLs Access Controls and permissions - Chapter 4 


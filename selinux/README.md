# SELinux

Ghori Chapter 21

RHCSA Objectives 53 - 57

```
Set enforcing and permissive modes for SELinux
List and identify SELinux file and process context
Restore default file contexts
Use boolean settings to modify system SELinux settings
Diagnose and address routine SELinux policy violations
```

see also:
- [Using SELinux on RHEL8](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_selinux/index)
- [Stop Disabling SELinux: A Real World Guide](https://www.sam.today/blog/stop-disabling-selinux:-a-real-world-guide/)

Sometimes, you want to do a thing, and SELinux gets in the way.
- If you're Red Team, that's the point.
- If you're Blue Team, you're happy about this.
- If you're neither, you need to twiddle something to make your thing work.

The very first test you should do, to confirm or deny your suspicion that "it's SELinux" or "it's the firewall" is turn them off, re-try the thing, and then turn them back on again (one at a time of course).

To *temporarily* turn SELinux from 'enforcing' to 'permissive', do this:

```bash
sudo setenforce 0
```

Then, test your previously failing operation. If it works, SELinux is the problem, and you have fixing to do.

To return to enforcing mode, do this:

```bash
sudo setenforce 1
```

To view the current SELinux state, do this:

```bash
getenforce
```

To view the boot-time SELinux state, do this:

```bash
less /etc/selinux/config
```

To change the boot-time SELinux state to 'enforcing', 'permissive', or 'disabled', you would modify `/etc/selinux/config`, but it is unlikely that you will need to do this for the exam. (i.e. SELinux should be 'enforcing' on boot when you start and finish)

To view detailed information about file and process contexts, do this:

```bash
sestatus -v
```

To view context of another file or process every time you run `sestatus -v`, add it to `/etc/sestatus.conf`.

To check the context of a file or process once, do this:

```bash
# get file context
ls -Z /path/to/file
# get process context
ps -eZ
```

---

Exploring and tweaking booleans and file and process contexts is fine, but:
- revert ineffectual changes
- commit effectual changes permanently

Ensure that your fix is sticky by relabeling, rebooting, and re-testing. Relabeling can be time-consuming, so it should be a final, "for good measure" measure.

To reboot and relabel, do this:

```bash
sudo touch /.autorelabel
sudo reboot
```

It took about a minute on my test system.

---

Use reference files/ports when setting context.

Same thing with a port context.

Same thing with a boolean.

```
sudo setsebool -P httpd_can_network_connect true
```

---

## Exercise 1 - Change port context

```bash
sudo dnf groupinstall "Basic Web Server"
sudo systemctl enable --now httpd
curl localhost
```

Browse or cURL from a remote host, note firewall problem.

```bash
# Open firewall temporarily on the usual port.
sudo firewall-cmd --zone=public --add-service=http
```

`semanage` is not installed by default in minimal and we're going to need it.

```bash
dnf provides "*bin/semanage"
sudo yum install policycoreutils-python-utils
sudo semanage port -l
```

Change the web-server to a weird port.

```bash
sudo sed -e 's/Listen 80/Listen 8888/' -i.bak /etc/httpd/conf/httpd.conf
diff /etc/httpd/conf/httpd.conf*
sudo systemctl restart httpd
sudo journalctl -xe
sudo systemctl status httpd

# try it with SELinux in permissive
sudo setenforce 0
sudo systemctl restart httpd
sudo systemctl status httpd
curl localhost:8888
# cURL or browse from another system
# open the port in the firewall
sudo firewall-cmd --permanent --zone=public --add-port=8888/tcp
sudo firewall-cmd --reload

# get the context of port 80 (which worked) and apply it to port 8888
sudo semanage port --list | grep 80
sudo semanage port --add --type http_port_t --proto tcp 8888
sudo semanage port --list | grep 8888
sudo setenforce 1
sudo systemctl restart httpd
```

## Exercise 2 - Change file context

Change the web-server to a weird directory.

```bash
sudo sed -e 's|/var/www|/srv/mysite|g' -i.bak /etc/httpd/conf/httpd.conf
diff /etc/httpd/conf/httpd.conf*
sudo mkdir -p /srv/mysite/html
echo "<html><body><p>Now I'm here.</p></body></html>" | sudo tee /srv/mysite/html/index.html
sudo systemctl restart httpd
curl localhost:8888
sudo systemctl status httpd
sudo journalctl -xe

# I can't find a log of the problem, but I can see that the fail-over page is being displayed.
sudo setenforce 0
curl localhost:8888
sudo setenforce 1
curl localhost:8888
sudo chcon --recursive --reference /var/www /srv/mysite
curl localhost:8888

# All seems well, but note what happens on a relabel
sudo restorecon -rv /srv
curl localhost:8888

# Now, make that context permanent by adding it to the file context registry
sudo semanage fcontext -a -t httpd_sys_content_t "/srv/mysite(/.*)?"
sudo restorecon -rv /srv
curl localhost:8888
```

## Exercise 3 - Use an SELinux boolean

This is a WIP!

Configure a public, writable NAS

```bash
# create the common shared directory
sudo mkdir /srv/commons
sudo chown 65534:65534 /srv/commons
# Install NFS server
# credit: https://linuxize.com/post/how-to-install-and-configure-an-nfs-server-on-centos-8/
sudo dnf install nfs-utils
echo '/srv/commons *(rw,all_squash,no_subtree_check,insecure,anonuid=65534,anongid=65534)' \
| sudo tee /etc/exports
sudo systemctl enable --now nfs-server
# Install Samba server
sudo yum install samba
echo '[commons]
    comment = Commons
    path = /srv/commons
    read only = No
    guest only = Yes
    guest ok = Yes' \
    | sudo tee /etc/samba/smb.conf
sudo systemctl enable --now smb
# test local mounts
sudo sudo mkdir /mnt/{nfs,smb}
sudo mount localhost:/srv/commons /mnt/nfs
touch /mnt/nfs/nfs-file
mkdir /mnt/nfs/nfs-dir
sudo dnf install cifs-utils
sudo mount -t cifs //localhost/commons /mnt/smb -o guest,noperm,uid=65534,gid=65534
# try to create test file/dir
touch /mnt/smb/smb-file
mkdir /mnt/smb/smb-dir
# verify that the problem is selinux
setenforce 0
touch /mnt/smb/smb-file
mkdir /mnt/smb/smb-dir
# revert the tests
rm /mnt/smb/smb-file
rmdir /mnt/smb/smb-dir
setenforce 1
# now, find the relevant booleans
getsebool -a | grep -i nfs
getsebool -a | grep -i smb
getsebool -a | grep -i samba
# note that some nfs booleans are already on. That must be why it works!
# test samba booleans by turning them on and off until you hit on the correct one
# smbd_anon_write and samba_export_all_rw seem likely
sudo setsebool -PV smbd_anon_write=on
touch /mnt/smb/smb-file
touch /mnt/smb/smb-dir
# Nope, that's not it. Let's revert that and try the other.
sudo setsebool -PV samba_export_all_rw=on
touch /mnt/smb/smb-file
touch /mnt/smb/smb-dir
# Now that SELinux is accounted for, we need to punch a couple holes in the firewall.
sudo firewall-cmd --list-all
sudo firewall-cmd --permanent --add-service=nfs
sudo firewall-cmd --permanent --add-service=samba
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
# And, mount the export and share from a remote system.
export MYSERVER=DLW-CentOS8 # You'll want to change that
sudo mkdir /mnt/{nfs,smb}
sudo mount $MYSERVER:/srv/commons /mnt/nfs
sudo mount.cifs //$MYSERVER/commons /mnt/smb -o guest,noperm,uid=65534,gid=65534
# And test it
touch /mnt/nfs/nfs-remote-file
mkdir /mnt/nfs/nfs-remote-dir
touch /mnt/smb/smb-remote-file
mkdir /mnt/smb/smb-remote-dir
ls -l /mnt/???/
```

Now, we have an anonymous NAS (file-server) that's compatible with Unix and Windows, and the firewall and SELinux are still enabled. Pretty cool.

Here's an article by Dan Walsh describing a much finer way to do this, using file contexts, instead of booleans for the Samba part.
https://danwalsh.livejournal.com/14195.html

Now, to find the same thing for NFS and turn all those nasty booleans back off.
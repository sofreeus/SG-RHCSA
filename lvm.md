# Logical Volume Management with LVM

Create a 2 machine study environment  

Practice with LVM:  
- add a disk 
- configure it as an LVM partition  

Use the commands:  
- pvcreate
- vgcreate or vgextend
- lvcreate and lvextend
- format file system: mkfs.xfs, or mkfs.ext4  
- add mount points to /etc/fstab  

Test, reboot, test some more.  

Try adding additional disk capacity to the volume, and resizing it with xfs_growfs.  

Also see:  [extend and reduce lvms in linux (external site)](https://www.tecmint.com/extend-and-reduce-lvms-in-linux/)

